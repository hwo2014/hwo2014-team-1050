import sys
import json

from hwo import Track

__author__ = 'alex'


def main():
    with open(sys.argv[1]) as f:
        track = None
        track_length = 0
        prev_piece = None
        displacements = []
        angles = []

        for line in f.readlines():
            if line.startswith('SEND') or line.startswith('RECV'):
                data = json.loads(line[5:].strip())

                if data['msgType'] == 'gameInit':
                    track = Track(data['data']['race']['track']['pieces'], data['data']['race']['track']['lanes'])

                if data['msgType'] == 'carPositions':
                    angles.append(data['data'][0]['angle'])

                    pos_data = data['data'][0]['piecePosition']

                    piece_index = pos_data['pieceIndex']
                    in_piece_dist = pos_data['inPieceDistance']
                    start_lane_index = pos_data['lane']['startLaneIndex']
                    end_lane_index = pos_data['lane']['endLaneIndex']
                    lap_no = pos_data['lap']

                    this_length = track.track_pieces[piece_index].get_lane_details(start_lane_index)['length']
                    if piece_index != prev_piece:
                        prev_piece = piece_index
                        track_length += this_length
                    displacements.append(track_length - this_length + in_piece_dist)


        speeds = []
        last = 0
        for s in displacements:
            speeds.append(s - last)
            last = s

        accelerations = []
        last = 0
        for v in speeds:
            accelerations.append(v - last)
            last = v

        data_out = []
        # data_out.append({'label': 'displacement', 'data': [[i, v] for i, v in enumerate(displacements)]})
        data_out.append({'label': 'speed', 'yaxis': 1, 'data': [[i, v] for i, v in enumerate(speeds)]})
        data_out.append({'label': 'acceleration', 'yaxis': 2, 'data': [[i, v] for i, v in enumerate(accelerations)]})
        data_out.append({'label': 'angle', 'yaxis': 3, 'data': [[i, v] for i, v in enumerate(angles)]})

        print "var data = ", json.dumps(data_out)


if __name__ == '__main__':
    main()
