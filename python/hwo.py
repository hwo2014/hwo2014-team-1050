import math
import pprint

pp = pprint.PrettyPrinter(indent=4)


class Track:
    def __init__(self, track_details, lane_details):
        self.lane_details = lane_details
        self.track_pieces = []

        for track_piece in track_details:
            has_crossover = 'switch' in track_piece and track_piece['switch'] == True
            if 'length' in track_piece:
                self.track_pieces.append(StraightTrack(track_piece['length'], has_crossover))
            else:
                self.track_pieces.append(
                    CurvedTrack(track_piece['angle'], track_piece['radius'], lane_details, has_crossover)
                )

    def get_lane_lengths_between_next_crossover_pair(self, piece_index):

        first_crossover = self.get_next_crossover(piece_index)
        if first_crossover is None:
            return None

        second_crossover = self.get_next_crossover(first_crossover)

        lane_lengths = {lane_detail['index']: 0 for lane_detail in self.lane_details}

        piece_index = first_crossover

        while piece_index != second_crossover:
            piece_index = self.get_next_piece_index(piece_index)
            for lane_index in lane_lengths.keys():
                lane_details = self.track_pieces[piece_index].get_lane_details(lane_index)
                # pp.pprint((lane_index, lane_details))
                lane_lengths[lane_index] += lane_details['length']

        # print "from %s to %s" % (first_crossover, second_crossover)
        # pp.pprint(lane_lengths)
        return lane_lengths

    def get_next_crossover(self, piece_index):
        current_piece_index = piece_index
        piece_index = self.get_next_piece_index(piece_index)
        while not self.track_pieces[piece_index].has_crossover:
            if piece_index == current_piece_index:
                return None
            piece_index = self.get_next_piece_index(piece_index)

        return piece_index

    def get_next_piece_index(self, piece_index):
        return (piece_index + 1) % len(self.track_pieces)

    def get_prev_piece_index(self, piece_index):
        return (len(self.track_pieces) + piece_index - 1) % len(self.track_pieces)


class TrackPiece:
    def __init__(self, has_crossover):
        self.has_crossover = has_crossover

    def get_lane_details(self, lane_index):
        """
        Must return a dict of {length, radius} where radius is None for non-curved track
        """
        raise NotImplementedError("This needs to be overridden in the subclass")


class CurvedTrack(TrackPiece):
    def __init__(self, angle, radius, lane_details, has_crossover=False):
        TrackPiece.__init__(self, has_crossover)
        self.angle = angle
        self.radius = radius
        self.lane_details = {}

        for lane_detail in lane_details:
            index = lane_detail['index']
            offset = lane_detail['distanceFromCenter']
            if self.angle < 0:
                offset = -offset
            self.lane_details[index] = {
                'length': abs(2 * math.pi * (self.radius - offset) * (self.angle / 360.0)),
                'radius': self.radius - offset,
            }

    def get_lane_details(self, lane_index):
        return self.lane_details[lane_index]


class StraightTrack(TrackPiece):
    def __init__(self, length, has_crossover=False):
        TrackPiece.__init__(self, has_crossover)
        self.length = length

    def get_lane_details(self, lane_index):
        return {'length': self.length, 'radius': None}
