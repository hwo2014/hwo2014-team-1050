import json
import socket
import sys
import time
import pprint

from hwo import Track

pp = pprint.PrettyPrinter(indent=4)

DATA_DIR = "data"

class NoobBot():

    def __init__(self, socket, name, key, track_name, log):
        self.socket        = socket
        self.name          = name
        self.key           = key
        self.track_name    = track_name

        self.prev_state    = None

        self.started       = False

        self.car_colour    = None
        self.race          = None
        self.track         = None

        self.current_lane   = 0
        self.intended_lane  = 0

        self.prev_angle     = 0.0
        self.dist           = 0.0

        if log:
            self.data_file = open("%s/%s" % (DATA_DIR, time.time()), 'w')
        else:
            self.data_file = None

    def log(self, type, msg):
        if self.data_file:
            self.data_file.write("%s:%s\n" % (type, msg))

    def msg(self, msg_type, data, tick=None):
        to_send = {"msgType": msg_type, "data": data}
        if tick is not None:
            to_send['gameTick'] = tick
        self.send(json.dumps(to_send))

    def send(self, msg):
        self.socket.sendall(msg + "\n")
        self.log('SEND', msg)

    def join(self):
        return self.msg("join", {
                            "name"     : self.name,
                            "key"      : self.key,
                            }
                )

    def create_race(self):
        return self.msg("createRace", {
                            "botId": {
                                "name"     : self.name,
                                "key"      : self.key,
                            },
                            "trackName": self.track_name,
                            "carCount" : 1,
                            }
                )



    def throttle(self, throttle, tick):
        self.msg("throttle", throttle, tick)

    def switch_lane(self, direction, tick):
        if direction not in ('Left', 'Right'):
            raise Exception("Put 'Left' or 'Right' you dumbass")
        print "Tick %s: turning %s" % (tick, direction)
        self.msg("switchLane", direction, tick)

    def ping(self, tick):
        self.msg("ping", {}, tick)

    def run(self):

        if self.track_name:
            self.create_race()
        else:
            self.join()

        self.msg_loop()

    def on_join(self, data, tick):
        print("Tick %s: Joined" % tick)
        self.ping(tick)

    def on_game_start(self, data, tick):
        print("Tick %s: Race started" % tick)
        self.started = True
        self.ping(tick)

    def get_car_velocity(self, data):
        prev_dist = self.dist
        self.prev_dist = prev_dist

        self.dist = data['piecePosition']['inPieceDistance']
        piece_index = data['piecePosition']['pieceIndex']
        start_lane = data['piecePosition']['lane']['startLaneIndex']
        end_lane = data['piecePosition']['lane']['endLaneIndex']

        if prev_dist > self.dist:
            prev_piece_num = self.track.get_prev_piece_index(piece_index)
            prev_piece = self.track.track_pieces[prev_piece_num]
            prev_piece_lane_length = prev_piece.get_lane_details(start_lane)['length']
            dist_diff = self.dist + (prev_piece_lane_length-self.prev_dist)

        else:
            dist_diff = self.dist-self.prev_dist

        self.velocity = dist_diff

    def on_car_positions(self, data, tick):

        # Change lanes if needed
        if not self.started:
            return

        for i, car_data in enumerate(data):
            if car_data['id']['color'] == self.car_colour:
                us = i
                break
        else:
            raise Exception("Can't find our car data (color: %s)" % (self.car_colour,))

        self.get_car_velocity(data[us])

        angle = abs(data[us]['angle'])
        piece_index = data[us]['piecePosition']['pieceIndex']
        piece_distance = data[us]['piecePosition']['inPieceDistance']
        self.current_lane = data[us]['piecePosition']['lane']['endLaneIndex']

        """
            Lane changing
        """
        if self.intended_lane == self.current_lane:  # if it's not, then probably already sent a switch

            distances = self.track.get_lane_lengths_between_next_crossover_pair(piece_index)
            min_dist = None
            min_indices = []
            for index, distance in distances.iteritems():  # TODO: Make this work for more than 2 lanes
                if min_dist is None:
                    min_dist = distance
                    min_indices.append(index)
                elif distance < min_dist:
                    min_dist = distance
                    min_indices = [index]
                elif distance == min_dist:
                    min_indices.append(index)

            # print "min_indices = %s, lane = %s, intended_lane = %s" % (min_indices, self.current_lane, self.intended_lane)
            if self.current_lane not in min_indices:
                # Might be worth changing lanes
                for index in min_indices:
                    if abs(index - self.current_lane) == 1:
                        if index - self.current_lane == 1:
                            self.switch_lane('Right', tick)
                            self.intended_lane = index
                            return
                        elif index - self.current_lane == -1:
                            self.switch_lane('Left', tick)
                            self.intended_lane = index
                            return

        #throttle=0.65
        prev_angle = self.prev_angle
        self.prev_angle = angle

        radius = self.track.track_pieces[piece_index].get_lane_details(self.current_lane)['radius']
        next_piece_index = self.track.get_next_piece_index(piece_index)
        radius_next = self.track.track_pieces[next_piece_index].get_lane_details(self.current_lane)['radius']

        if radius and radius < 91.0:
            if self.velocity > 6.50 or angle > 50.0:
                throttle = 0.35
            elif angle > prev_angle:
                throttle = 0.55
            else:
                if angle < 45:
                    throttle = 1.0
                else:
                    throttle=0.65
        elif angle < 5.0 and (not radius_next or radius_next > 91.0):
            throttle = 0.8
        else:
            if radius_next and radius_next < 91.0 and self.velocity > 7.5:
                throttle=0.6
            else:
                throttle=0.7
            #throttle = 0.7

        self.throttle(throttle, tick)

        if log:
            print "Tick "+str(tick)+": Throttle: "+str(throttle)+" Prev: "+str(prev_angle)+" Angle: "+str(angle)+" Velocity: "+str(self.velocity)+" PieceRadius: "+str(radius)
        return
        min_damp_angle = 20
        max_damp_angle = 40
        max_throttle = 0.70

        throttle=0
        prev_angle = self.prev_angle
        self.prev_angle = angle

        if angle == 0:
            throttle = 0.9
        elif angle < min_damp_angle:
            throttle = max_throttle
        #elif angle > max_damp_angle:
        #    throttle = 0.10
        elif (angle-prev_angle)>4.0:
            throttle = 0.1
        else:
            k = 1.0/(max_damp_angle **2)
            diff = 1.0
            if angle < prev_angle:
                diff = 1.18
                #diff = 1.0 + ((prev_angle-angle)**(1/2.0))/5.0
            mult = 1.0-k*((angle - min_damp_angle)**2)
            throttle = mult*max_throttle*diff
            if throttle > 0.99:
                throttle = 1.0
            elif throttle < 0.01:
                throttle = 0.0
        #if abs(angle) < 20:
        #	self.throttle(0.7) #(0.6568)
        #else:
        #	self.throttle(0.47)
        # print "Throttle: "+str(throttle)+" Prev: "+str(prev_angle)+" Angle: "+str(angle)+" Velocity: "+str(self.velocity)
        self.throttle(throttle, tick)

        self.prev_state = data

    def on_crash(self, data, tick):
        print("Tick %s: %s crashed" % (tick, data['color']))
        self.ping(tick)

    def on_game_end(self, data, tick):
        print "Tick %s: Race ended" % tick
        self.started = False
        self.ping(tick)

    def on_error(self, data, tick):
        print("Error: {0}".format(data))
        self.ping(tick)

    def on_game_init(self, data, tick):
        print "Game init"
        self.race = data['race']
        self.track = Track(self.race['track']['pieces'], self.race['track']['lanes'])

    def on_your_car(self, data, tick):
        self.car_colour = data['color']
        self.ping(tick)

    def on_lap_finished(self, data, tick):
        print "Tick %s: lap time for %s: %s ms" % (tick, data['car']['color'], data['lapTime']['millis'])
        self.ping(tick)

    def on_spawn(self, data, tick):
        print "Tick %s: %s spawned" % (tick, data['color'])
        self.ping(tick)

    def on_finish(self, data, tick):
        print "Tick %s: %s finished" % (tick, data['color'])
        self.ping(tick)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'lapFinished': self.on_lap_finished,
            'spawn': self.on_spawn,
			'finish': self.on_finish,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            self.log('RECV', line.rstrip())
            msg = json.loads(line)
            msg_type, data, tick = msg['msgType'], msg['data'], None if 'gameTick' not in msg else msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data, tick)
            else:
                print("Tick %s: Got %s" % (tick, msg_type))
                self.ping(tick)
            line = socket_file.readline()


def run_bot(source, name, key, track, log=False):

    bot = NoobBot(source, name, key, track, log)
    bot.run()

if __name__ == "__main__":

    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey [track_name]")
    else:
        host, port, name, key = sys.argv[1:5]

        if len(sys.argv) >= 6:
            track_name = sys.argv[5]
        else:
            track_name = None

        log = track_name not in (None, '')  # Only log if we've specified a track (will hopefully run quicker on production)

        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))

        run_bot(s, name, key, track_name, log)


