"""
    This is a really dumb server

    You give it a log file (FAKE_DATA_SOURCE) and
    it will just return that to the client until
    it finishes.

    It depends upon the lines in the log files starting
    with SEND: and RECV:

    It doesn't do anything clever. At all.

"""

FAKE_DATA_SOURCE = "data/1397597271.99"


class FakeServer(object):

    def __init__(self):
        self.source = open(FAKE_DATA_SOURCE, 'r')

    def readline(self):
        while True:
            line = self.source.readline()
            if not line:
                return None
            if line[:5] == "RECV:":
                return line[5:]

    def send(self, msg):
        pass

    def sendall(self, msg):
        pass


    def makefile(self):
        return self
